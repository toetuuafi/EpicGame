using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoamingSceneManager : MonoBehaviour
{
    GameManager gameManager;
    OutsideLevelRenderer levelRenderer;
    void Awake()
    {
        levelRenderer = GetComponent<OutsideLevelRenderer>();
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        gameManager.RequestRoamingSceneInfo(gameObject);
    }
    public void SetupScene(PlayerSpriteData _playerSpriteData, RoamingSceneData _roamingSceneData, Vector2 PlayerSpawnPos)
    {
        levelRenderer.RenderFloor(_roamingSceneData.FloorMap);

        //Spawning Player
        GameObject Player = (GameObject)Instantiate(Resources.Load("Prefabs/RoamingPlayer"));
        Player.transform.position = PlayerSpawnPos;
        GameObject.FindGameObjectWithTag("MainCamera").transform.position = new Vector3(PlayerSpawnPos.x, PlayerSpawnPos.y, -10);
    }
}
