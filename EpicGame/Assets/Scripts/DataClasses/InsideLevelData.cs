using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsideLevelData
{
    public int Background;
    public Vector2 SpawnPos;
    public Vector2 MoveLimitTopLeft;
    public Vector2 MoveLimitBottomRight;
}
