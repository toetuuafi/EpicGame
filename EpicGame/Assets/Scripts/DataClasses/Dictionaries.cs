using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dictionaries : MonoBehaviour
{
    //Roaming floor tile sprites
    public Dictionary<int, Sprite> RoamingFloorTiles = new Dictionary<int, Sprite>();
    //Hexadec colour values dictionary
    public Dictionary<string, int> ColourToTextureDict = new Dictionary<string, int>();
    //Player sprite data dictionary
    public Dictionary<int, PlayerSpriteData> PlayerSpriteSets = new Dictionary<int, PlayerSpriteData>();
    //Roaming Scene Data Dictionary
    public Dictionary<int, RoamingSceneData> RoamingLevels = new Dictionary<int, RoamingSceneData>();
    //Inside Scene Room Data Dictionary
    public Dictionary<int, InsideLevelData> InsideLevels = new Dictionary<int, InsideLevelData>();
    private void Start()
    {
        //Roaming floor tile sprites
        RoamingFloorTiles[0] = Resources.Load<Sprite>("Stone");

        //Player sprite data dictionary
        PlayerSpriteSets[0] = new PlayerSpriteData
        {
            RoamingSprites = new Dictionary<int, Sprite>
            {
                [0] = Resources.Load<Sprite>("Characters/Roaming/Char1/Roaming/Head"),
                [1] = Resources.Load<Sprite>("Characters/Roaming/Char1/Roaming/BIdle"),
                [2] = Resources.Load<Sprite>("Characters/Roaming/Char1/Roaming/BR1"),
                [3] = Resources.Load<Sprite>("Characters/Roaming/Char1/Roaming/BR2"),
                [4] = Resources.Load<Sprite>("Characters/Roaming/Char1/Roaming/BR3"),
            },
            InsideSprites = new Dictionary<int, Sprite>
            {
                [0] = Resources.Load<Sprite>("Characters/Char1/Inside")
            }
        };

        //Roaming Scene Data Dictionary
        RoamingLevels[0] = new RoamingSceneData
        {
            FloorMap = Resources.Load<Texture2D>("Stone")
        };

        //Hexadec colour values dictionary
        ColourToTextureDict = new Dictionary<string, int>
        {
            ["b5b5b5"] = 0
        };

        InsideLevels[0] = new InsideLevelData
        {
            Background = 0,
            MoveLimitTopLeft = new Vector2(-9.4f,-0.8f),
            MoveLimitBottomRight = new Vector2(9.2f,-3.0f),
            SpawnPos = new Vector2(0, -3)
        };

    }
}
