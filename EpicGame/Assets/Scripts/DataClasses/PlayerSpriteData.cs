using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpriteData
{
    //Sprite List: 0 head, 1 body idle, 2 - 3 Body running animation
    public Dictionary<int,Sprite> RoamingSprites = new Dictionary<int, Sprite>();
    public Dictionary<int, Sprite> InsideSprites = new Dictionary<int, Sprite>();
}
