using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsideSceneBuilder : MonoBehaviour
{
    public SpriteRenderer Background;
    InsideSceneLevelDictionary dictionary;

    private void Awake()
    {
        dictionary = GetComponent<InsideSceneLevelDictionary>();
    }
    public void SetupScene(InsideLevelData LevelData)
    {
        Background.sprite = GetComponent<InsideSceneLevelDictionary>().BackgroundDictionary[LevelData.Background];
    }
}
