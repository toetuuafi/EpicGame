using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsidePlayer : MonoBehaviour
{
    Rigidbody2D body;
    SpriteRenderer sprite;

    float horizontal;
    float vertical;

    Vector2 MoveLimitTopLeft;
    Vector2 MoveLimitBottomRight;

    public float runSpeed = 20.0f;
    const float moveLimiter = 0.7f;

    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");
    }
    public void FixedUpdate()
    {
        //Moving Limits
        if (vertical > 0 && transform.position.y >= MoveLimitTopLeft.y)
            vertical = 0;
        if (vertical < 0 && transform.position.y <= MoveLimitBottomRight.y)
            vertical = 0;

        if (horizontal != 0 && vertical != 0)
        {
            horizontal *= moveLimiter;
            vertical *= moveLimiter;
        }
        body.velocity = new Vector2(horizontal * runSpeed, vertical * runSpeed);
    }

    public void SetLims(Vector2 _topleft, Vector2 _bottomright)
    {
        MoveLimitTopLeft = _topleft;
        MoveLimitBottomRight = _bottomright;
    }
}
