using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorEnterDetect : MonoBehaviour
{
    bool enterzone;
    public int RoomToEnter;
    GameManager gameManager;

    private void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }
    private void FixedUpdate()
    {
        if (Input.GetAxis("Vertical") > 0 && enterzone)
        {
            gameManager.ChangeToInsideScene(gameManager.Dictionaries.InsideLevels[RoomToEnter]);
            enterzone = false;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        enterzone = true;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        enterzone = false;
    }
}
