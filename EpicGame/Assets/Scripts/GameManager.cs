using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    PlayerSpriteData playerSpriteData;
    RoamingSceneData NextRoamingScene;
    Vector2 NextPlayerSpawnPos;
    public Dictionaries Dictionaries;

    InsideLevelData NextInsideSceneData;

    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("GameManager");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }

        DontDestroyOnLoad(this.gameObject);
    }
    public void ChangeToRoamingScene(RoamingSceneData _roamingSceneData, Vector2 PlayerSpawnPos)
    {
        Debug.Log("Loading Scene");
        NextRoamingScene = _roamingSceneData;
        NextPlayerSpawnPos = PlayerSpawnPos;
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }
    public void ChangeToInsideScene(InsideLevelData _insideSceneData)
    {
        Debug.Log("Loading Scene");
        NextInsideSceneData = _insideSceneData;
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }
    //Coms with RoamingLevelManager
    public void SetPlayerSpriteData(int _character)
    {
        playerSpriteData = Dictionaries.PlayerSpriteSets[_character];
    }
    public void RequestRoamingSceneInfo(GameObject _sceneManager)
    {
        _sceneManager.GetComponent<RoamingSceneManager>().SetupScene(playerSpriteData, NextRoamingScene, NextPlayerSpawnPos);
    }

    //Coms with InsideLevelManager
    public void RequestInsideSceneInfo(GameObject _sceneManager)
    {
        _sceneManager.GetComponent<InsideSceneManager>().SetupScene(playerSpriteData, NextInsideSceneData);
    }
}
