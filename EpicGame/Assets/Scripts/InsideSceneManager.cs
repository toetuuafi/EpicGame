using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsideSceneManager : MonoBehaviour
{
    GameManager gameManager;
    InsideSceneBuilder scenebuilder;
    public GameObject PlayerPrefab;
    public void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        scenebuilder = GetComponent<InsideSceneBuilder>();
        gameManager.RequestInsideSceneInfo(gameObject);
    }

    public void SetupScene(PlayerSpriteData playerSpriteData, InsideLevelData LevelData)
    {
        scenebuilder.SetupScene(LevelData);

        var player = Instantiate(PlayerPrefab);
        player.transform.position = LevelData.SpawnPos;
        player.GetComponent<InsidePlayer>().SetLims(LevelData.MoveLimitTopLeft, LevelData.MoveLimitBottomRight);
    }
}
