using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutsidePlayerAnimationHandler : MonoBehaviour
{
    public List<Sprite> SpriteList;
    public float ticksBetweenFrame;

    private int RunningCounter;
    private SpriteRenderer spriteRenderer;

    public float HeadBobAmplitude = 0.5f;
    public float HeadBobFrequency = 1f;

    public Transform HeadTransform;
    
    private bool running;

    public void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }
    public void StartRunning()
    {
        running = true;
        
    }
    public void StartIdle()
    {
        running = false;
        spriteRenderer.sprite = SpriteList[0];
    }
    public void FixedUpdate()
    {
        //Running animation
        if (running)
        {
            RunningCounter++;
            if (RunningCounter >= ticksBetweenFrame)
            {
                if (spriteRenderer.sprite == SpriteList[0])
                {
                    spriteRenderer.sprite = SpriteList[1];
                }
                else if (spriteRenderer.sprite == SpriteList[1])
                {
                    spriteRenderer.sprite = SpriteList[2];
                }
                else if (spriteRenderer.sprite == SpriteList[2])
                {
                    spriteRenderer.sprite = SpriteList[3];
                }
                else if (spriteRenderer.sprite == SpriteList[3])
                {
                    spriteRenderer.sprite = SpriteList[1];
                }
                RunningCounter = 0;
            }
        }
    }
    public void Update()
    {
        if(running)
        HeadTransform.position = new Vector2(HeadTransform.position.x, transform.position.y + (Mathf.Sin(Time.fixedTime * Mathf.PI * HeadBobFrequency) * HeadBobAmplitude));
    }
}
