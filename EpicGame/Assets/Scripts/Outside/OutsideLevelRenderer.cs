using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class OutsideLevelRenderer : MonoBehaviour
{
    public float scale;
    GameManager gameManager;
    public Texture2D LevelFloorImg;
    public void Awake()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }
    //Converts an image to a 2D array
    private int[,] Texture2DtoPixelArray(Texture2D image)
    {
        Debug.Log("Reading Floor Map Pixel Values");

        var i = new int[image.width, image.height];
        
        for(int x = 0; x < image.width; x++)
        {
            for (int y = 0; y < image.height; y++)
            {
                var pixel = image.GetPixel(x, y);
                string rgb = ColorUtility.ToHtmlStringRGB(pixel).ToLower();
                try
                {
                    i[x, y] = gameManager.Dictionaries.ColourToTextureDict[rgb];
                }
                catch
                {
                    i[x, y] = -1;
                }
            }
        }
        //Debug.Log(i);

        return i;
    }
    private GameObject CreateFloor(int[,] TileArray)
    {
        var floorObject = new GameObject();
        floorObject.name = "Floor";
        //Instantiate(floorObject);
        for (int x = 0; x < TileArray.GetLength(0); x++)
        {
            for (int y = 0; y < TileArray.GetLength(1); y++)
            {
                if (TileArray[x, y] != -1)
                {
                    try
                    {
                        GameObject i = new GameObject();
                        i.transform.parent = floorObject.transform;
                        i.AddComponent<SpriteRenderer>().sprite = gameManager.Dictionaries.RoamingFloorTiles[TileArray[x, y]];
                        i.transform.position = new Vector3(x, y, 0);
                        i.transform.localScale = new Vector3(1 * scale, 1 * scale, 1);
                        i.name = "Floor Tile: " + x + ", " + y;
                    }
                    catch
                    {
                    }
                }
            }
        }
        return floorObject;
    }

    public void RenderFloor(Texture2D tilemap)
    {
        Debug.Log("Rendering Floor");
        int[,] TileArray = Texture2DtoPixelArray(tilemap);
        var Floor = CreateFloor(TileArray);
    }
}
