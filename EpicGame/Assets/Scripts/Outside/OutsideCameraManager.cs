using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutsideCameraManager : MonoBehaviour
{
    public float yoffsetPlayer;
    public float Delay;
    public float Speed;
    public float StopFollowingRange;

    Rigidbody2D CamRb;

    GameObject player;
    Transform PlayerTransform;
    OutsidePlayer playerScript;

    private bool Following;
    // Start is called before the first frame update
    public void Start()
    {
        CamRb = GetComponent<Rigidbody2D>();
        StartFollowing();
    }

    public void StartFollowing()
    {
        Following = true;
        player = GameObject.FindGameObjectWithTag("Player");
        PlayerTransform = player.transform;
        playerScript = player.GetComponent<OutsidePlayer>();
    }

    // Update is called once per frame
    public void FixedUpdate()
    {
        if(Following)
        {
            //Make camera follow player
            if (Vector2.Distance(PlayerTransform.position, transform.position) > StopFollowingRange)
            {
                Speed = playerScript.runSpeed;
                Vector2 direction = PlayerTransform.position - transform.position;
                Vector2 newvector = direction.normalized * Speed;
                CamRb.velocity = newvector;
            }
        }
    }
}
