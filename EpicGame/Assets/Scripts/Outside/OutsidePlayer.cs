using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OutsidePlayer : MonoBehaviour
{
    Rigidbody2D body;
    SpriteRenderer sprite;
    OutsidePlayerAnimationHandler animationHandler;
    public SpriteRenderer Head;

    float horizontal;
    float vertical;

    public float runSpeed = 20.0f;
    const float moveLimiter = 0.7f;

    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();
        animationHandler = GetComponent<OutsidePlayerAnimationHandler>();
    }

    void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");

        switch (horizontal)
        {
            case -1:
                sprite.flipX = false;
                Head.flipX = false;
                break;
            case 1:
                sprite.flipX = true;
                Head.flipX = true;
                break;
        }

        if (horizontal != 0 || vertical != 0)
        {
            animationHandler.StartRunning();
        } else
        {
            animationHandler.StartIdle();
        }
    }
    public void FixedUpdate()
    {
        if (horizontal != 0 && vertical != 0) // Check for diagonal movement
        {
            // limit movement speed diagonally, so you move at 70% speed
            horizontal *= moveLimiter;
            vertical *= moveLimiter;
        }
        body.velocity = new Vector2(horizontal * runSpeed, vertical * runSpeed);
    }
    public void InitPlayer(PlayerSpriteData _playerspritedata, Vector2 _spawnPos)
    {
        animationHandler.SpriteList[0] = _playerspritedata.RoamingSprites[1];
        animationHandler.SpriteList[1] = _playerspritedata.RoamingSprites[2];
        animationHandler.SpriteList[2] = _playerspritedata.RoamingSprites[3];
        animationHandler.SpriteList[3] = _playerspritedata.RoamingSprites[4];
        Head.sprite = _playerspritedata.RoamingSprites[0];
        transform.position = _spawnPos;
    }
}
