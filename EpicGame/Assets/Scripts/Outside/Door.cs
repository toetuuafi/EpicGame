using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite Open, Closed;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        spriteRenderer.sprite = Open;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        spriteRenderer.sprite = Closed;
    }
}
