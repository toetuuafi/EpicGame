using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneLoadTest : MonoBehaviour
{
    public GameManager gameManager;
    public void Roam()
    {
        var pos = new Vector2(3,3);
        gameManager.SetPlayerSpriteData(0);
        gameManager.ChangeToRoamingScene(gameManager.Dictionaries.RoamingLevels[0], pos);
    }
    public void inside()
    {
        gameManager.SetPlayerSpriteData(0);
        gameManager.ChangeToInsideScene(gameManager.Dictionaries.InsideLevels[0]);
    }
}
